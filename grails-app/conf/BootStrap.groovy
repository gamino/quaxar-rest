import webservicetest_two_three_eleven.Boat
import webservicetest_two_three_eleven.Book

class BootStrap {

    def init = { servletContext ->
        new Book(title: "The Stand", author: "Ariel").save()
        new Book(title: "The Shining", author: "Joe").save()
        new Boat(boatID:33,boatName:"La Catrina",marina:"Miami",make:"Kawasaki",model:"UT23",length:"34",year:2009,engine:"34k",propulsion:"23HP",fuelCapacity:"15 Gallons",fuelType:"Gas",category:"Family",activities:"n/a",referralID:101).save()
        new Boat(boatID:34,boatName:"La Cucaracha",marina:"Key Byscaine",make:"Ford",model:"T2",length:"24",year:2012,engine:"3",propulsion:"15HP",fuelCapacity:"15 Gallons",fuelType:"Gas",category:"Family",activities:"n/a",referralID:154).save()


    }
    def destroy = {
    }
}
