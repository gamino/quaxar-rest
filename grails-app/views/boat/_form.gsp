<%@ page import="webservicetest_two_three_eleven.Boat" %>



<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'activities', 'error')} required">
	<label for="activities">
		<g:message code="boat.activities.label" default="Activities" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="activities" required="" value="${boatInstance?.activities}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'boatID', 'error')} required">
	<label for="boatID">
		<g:message code="boat.boatID.label" default="Boat ID" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="boatID" type="number" value="${boatInstance.boatID}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'boatName', 'error')} required">
	<label for="boatName">
		<g:message code="boat.boatName.label" default="Boat Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="boatName" required="" value="${boatInstance?.boatName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'category', 'error')} required">
	<label for="category">
		<g:message code="boat.category.label" default="Category" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="category" required="" value="${boatInstance?.category}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'engine', 'error')} required">
	<label for="engine">
		<g:message code="boat.engine.label" default="Engine" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="engine" required="" value="${boatInstance?.engine}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'fuelCapacity', 'error')} required">
	<label for="fuelCapacity">
		<g:message code="boat.fuelCapacity.label" default="Fuel Capacity" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="fuelCapacity" required="" value="${boatInstance?.fuelCapacity}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'fuelType', 'error')} required">
	<label for="fuelType">
		<g:message code="boat.fuelType.label" default="Fuel Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="fuelType" required="" value="${boatInstance?.fuelType}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'length', 'error')} required">
	<label for="length">
		<g:message code="boat.length.label" default="Length" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="length" required="" value="${boatInstance?.length}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'make', 'error')} required">
	<label for="make">
		<g:message code="boat.make.label" default="Make" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="make" required="" value="${boatInstance?.make}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'marina', 'error')} required">
	<label for="marina">
		<g:message code="boat.marina.label" default="Marina" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="marina" required="" value="${boatInstance?.marina}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'model', 'error')} required">
	<label for="model">
		<g:message code="boat.model.label" default="Model" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="model" required="" value="${boatInstance?.model}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'propulsion', 'error')} required">
	<label for="propulsion">
		<g:message code="boat.propulsion.label" default="Propulsion" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="propulsion" required="" value="${boatInstance?.propulsion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'referralID', 'error')} required">
	<label for="referralID">
		<g:message code="boat.referralID.label" default="Referral ID" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="referralID" type="number" value="${boatInstance.referralID}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: boatInstance, field: 'year', 'error')} required">
	<label for="year">
		<g:message code="boat.year.label" default="Year" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="year" type="number" value="${boatInstance.year}" required=""/>

</div>

