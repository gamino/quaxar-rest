
<%@ page import="webservicetest_two_three_eleven.Boat" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'boat.label', default: 'Boat')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-boat" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-boat" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list boat">
			
				<g:if test="${boatInstance?.activities}">
				<li class="fieldcontain">
					<span id="activities-label" class="property-label"><g:message code="boat.activities.label" default="Activities" /></span>
					
						<span class="property-value" aria-labelledby="activities-label"><g:fieldValue bean="${boatInstance}" field="activities"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.boatID}">
				<li class="fieldcontain">
					<span id="boatID-label" class="property-label"><g:message code="boat.boatID.label" default="Boat ID" /></span>
					
						<span class="property-value" aria-labelledby="boatID-label"><g:fieldValue bean="${boatInstance}" field="boatID"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.boatName}">
				<li class="fieldcontain">
					<span id="boatName-label" class="property-label"><g:message code="boat.boatName.label" default="Boat Name" /></span>
					
						<span class="property-value" aria-labelledby="boatName-label"><g:fieldValue bean="${boatInstance}" field="boatName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.category}">
				<li class="fieldcontain">
					<span id="category-label" class="property-label"><g:message code="boat.category.label" default="Category" /></span>
					
						<span class="property-value" aria-labelledby="category-label"><g:fieldValue bean="${boatInstance}" field="category"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.engine}">
				<li class="fieldcontain">
					<span id="engine-label" class="property-label"><g:message code="boat.engine.label" default="Engine" /></span>
					
						<span class="property-value" aria-labelledby="engine-label"><g:fieldValue bean="${boatInstance}" field="engine"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.fuelCapacity}">
				<li class="fieldcontain">
					<span id="fuelCapacity-label" class="property-label"><g:message code="boat.fuelCapacity.label" default="Fuel Capacity" /></span>
					
						<span class="property-value" aria-labelledby="fuelCapacity-label"><g:fieldValue bean="${boatInstance}" field="fuelCapacity"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.fuelType}">
				<li class="fieldcontain">
					<span id="fuelType-label" class="property-label"><g:message code="boat.fuelType.label" default="Fuel Type" /></span>
					
						<span class="property-value" aria-labelledby="fuelType-label"><g:fieldValue bean="${boatInstance}" field="fuelType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.length}">
				<li class="fieldcontain">
					<span id="length-label" class="property-label"><g:message code="boat.length.label" default="Length" /></span>
					
						<span class="property-value" aria-labelledby="length-label"><g:fieldValue bean="${boatInstance}" field="length"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.make}">
				<li class="fieldcontain">
					<span id="make-label" class="property-label"><g:message code="boat.make.label" default="Make" /></span>
					
						<span class="property-value" aria-labelledby="make-label"><g:fieldValue bean="${boatInstance}" field="make"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.marina}">
				<li class="fieldcontain">
					<span id="marina-label" class="property-label"><g:message code="boat.marina.label" default="Marina" /></span>
					
						<span class="property-value" aria-labelledby="marina-label"><g:fieldValue bean="${boatInstance}" field="marina"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.model}">
				<li class="fieldcontain">
					<span id="model-label" class="property-label"><g:message code="boat.model.label" default="Model" /></span>
					
						<span class="property-value" aria-labelledby="model-label"><g:fieldValue bean="${boatInstance}" field="model"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.propulsion}">
				<li class="fieldcontain">
					<span id="propulsion-label" class="property-label"><g:message code="boat.propulsion.label" default="Propulsion" /></span>
					
						<span class="property-value" aria-labelledby="propulsion-label"><g:fieldValue bean="${boatInstance}" field="propulsion"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.referralID}">
				<li class="fieldcontain">
					<span id="referralID-label" class="property-label"><g:message code="boat.referralID.label" default="Referral ID" /></span>
					
						<span class="property-value" aria-labelledby="referralID-label"><g:fieldValue bean="${boatInstance}" field="referralID"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${boatInstance?.year}">
				<li class="fieldcontain">
					<span id="year-label" class="property-label"><g:message code="boat.year.label" default="Year" /></span>
					
						<span class="property-value" aria-labelledby="year-label"><g:fieldValue bean="${boatInstance}" field="year"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:boatInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${boatInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
