
<%@ page import="webservicetest_two_three_eleven.Boat" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'boat.label', default: 'Boat')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-boat" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-boat" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="activities" title="${message(code: 'boat.activities.label', default: 'Activities')}" />
					
						<g:sortableColumn property="boatID" title="${message(code: 'boat.boatID.label', default: 'Boat ID')}" />
					
						<g:sortableColumn property="boatName" title="${message(code: 'boat.boatName.label', default: 'Boat Name')}" />
					
						<g:sortableColumn property="category" title="${message(code: 'boat.category.label', default: 'Category')}" />
					
						<g:sortableColumn property="engine" title="${message(code: 'boat.engine.label', default: 'Engine')}" />
					
						<g:sortableColumn property="fuelCapacity" title="${message(code: 'boat.fuelCapacity.label', default: 'Fuel Capacity')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${boatInstanceList}" status="i" var="boatInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${boatInstance.id}">${fieldValue(bean: boatInstance, field: "activities")}</g:link></td>
					
						<td>${fieldValue(bean: boatInstance, field: "boatID")}</td>
					
						<td>${fieldValue(bean: boatInstance, field: "boatName")}</td>
					
						<td>${fieldValue(bean: boatInstance, field: "category")}</td>
					
						<td>${fieldValue(bean: boatInstance, field: "engine")}</td>
					
						<td>${fieldValue(bean: boatInstance, field: "fuelCapacity")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${boatInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
