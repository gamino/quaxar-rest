package webservicetest_two_three_eleven
import grails.rest.Resource

@Resource(uri='/books', formats=['json','xml'])
class Book {
    String title
    String author
    static constraints = {
    }
}
