package webservicetest_two_three_eleven
import grails.rest.Resource

@Resource(uri='/books', formats=['json','xml'])
class Boat {
    Integer boatID
    String boatName
    String marina
    String make
    String model
    String length
    Integer year
    String engine
    String propulsion
    String fuelCapacity
    String fuelType
    String category
    String activities
    Integer referralID
    static constraints = {

    }
}
