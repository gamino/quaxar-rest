package webservicetest_two_three_eleven



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class BoatController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Boat.list(params), model: [boatInstanceCount: Boat.count()]
    }

    def show(Boat boatInstance) {
        respond boatInstance
    }

    def create() {
        respond new Boat(params)
    }

    @Transactional
    def save(Boat boatInstance) {
        if (boatInstance == null) {
            notFound()
            return
        }

        if (boatInstance.hasErrors()) {
            respond boatInstance.errors, view: 'create'
            return
        }

        boatInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'boat.label', default: 'Boat'), boatInstance.id])
                redirect boatInstance
            }
            '*' { respond boatInstance, [status: CREATED] }
        }
    }

    def edit(Boat boatInstance) {
        respond boatInstance
    }

    @Transactional
    def update(Boat boatInstance) {
        if (boatInstance == null) {
            notFound()
            return
        }

        if (boatInstance.hasErrors()) {
            respond boatInstance.errors, view: 'edit'
            return
        }

        boatInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Boat.label', default: 'Boat'), boatInstance.id])
                redirect boatInstance
            }
            '*' { respond boatInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Boat boatInstance) {

        if (boatInstance == null) {
            notFound()
            return
        }

        boatInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Boat.label', default: 'Boat'), boatInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'boat.label', default: 'Boat'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
